-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.19 (Darwin)

mQINBFCGGC4BEACzfTor+hr3ytxbcDg8NM1ix41Sshb/fLP2aZ7wM1OCRuzw8ZxJ
IO7gZAAiNA+M8nL1AM7Cmbi+g/dPAUMKXud0/QgFvQ0x2ekGUi6C85fKbYe5jRhM
GABWRW2FABm9+rs9jdSSaA1plKZ+15UfttVDqEPgz6kR96Flx4jhRLRPmYZnbNgH
7erLhq+mwMlJmmVZ5Aolb6R1dtcX1m4vGoQx2PM79aiQJUDstLeq8GIGMA+Zhm1t
vpQRYlZKOpBXFlDku0OXl+lBuCxOvlFF8FPPfhBpFNorcOX/bLGqnho5oEV4F4Kc
I7RVioGIMgNEbIPZWbcd8N8pBo3MEAq9iusB06mPUuK4XAiWrVMXDrjrkCSQm7tO
tE7lbICUJacX0R2PdyJrsanWI1UpuMJ2j1x2pXogyVn2C+vjeJRy59uGpvbIPr6/
seorv0UmTA+1VlL3iR9I7NensH1yjgHrdBBcyiCNIzlSacmh4ElfsdqfYpVL9LZZ
j4VdrlAQHN/KDAlQS4tehEvuo36xx0uDreen6jng0cpfCmbBU7oo5brLSIgX5K1P
7nqqWPDdKvYsKrVt7quWdf3CEdBCQF8P1OZL2NTArYWL9MwBIQ5ohlNmcVqpYSe3
NsDNAu9rFEx+UCDxJq0fKgrENPFbzkYAfIORzdIzgYYzzldJ3GwTH8Ei5QARAQAB
tAlDaHJpcyBPZWmJAjgEEwECACIFAlCGGC4CGwMGCwkIBwMCBhUIAgkKCwQWAgMB
Ah4BAheAAAoJELAqvr+A71ujM4EP/RQ12PIf8ySzSPYSXgxPEhPEtHnvvqLNuzqj
Cb5GBxFjDXJNnG1/edTRrGWETi0e+v5aFkndZrCxpodzrjRwjuv5ohfIfRitziqm
M039a7idjHWRNR78RaBxPmdW2oEoxXf2jXiVmxpLGZoaGgHZsD2WGaD+gQG6RTbq
D+M0XaJK2aYkLa5XtnCNr/5pYYbaaUw9iEN2CeFcsUZaxfgk1UuMKIFQ+db0G4rc
s2cL6Uq+2XmPeCW/Wy0FwjpyLaeKM8D+7ipVZPRr4wU7gm+XMAQddi1lIkwRpbqL
qg4/UAACgn43+iqgoLmRnD3ZLoqmdOeQuWQ0iScaIDWSRqw0gPgYXd5Z3Q7DX0aU
hqJ4u16GP5oUk445nuVvo07sT0rMq/1kGd6Csfk6/52SL+4WvVn2uSKYJzLe7zOT
FZr3jslWt13PUusigXPd1N6XaNLslk9qDQvHcOrI03GRIvaQl2vvxk4UCa97e/KF
Q2MaknR45YR16QXJCZO228FBynwQmy+Xv2NYh9thOlLta/ZI/Zx7qTAO5nbkcV53
D0lxz/uZfO4ZiiZKos1NoYVS2dC1IeUJ/2fFC4a2d7Dts5xnKsL7P22FCgU/q10F
B0oZL1JiryKu01bmr/iJcvvGVdD2LOiL0s+nsO2/k/tqTZAlGPL7HU8DiYSJKkLi
6ZIu76J1uQINBFCGGC4BEAC8QPZSegAlVOta+Ayqlr1GCzyNph3eFEdj37Osnueb
/eXZZlVShmI7bE7aPEloBuqIJpyGx7raV8LuZsxcjpTcKf1PPScioiap8Ma3aZvi
WYjyDjAZCom/YrLbcdDSVrD6jf07oN1FdJ3O/tZve6POmEFREEUtVhxwPa2MQZnZ
VJlNvDygL4Fut04wgNQ/C5IPf98iisXqLc/OPDBTf3dvNq+IQEJ/p6MeKxN3NBaq
xjF3ATl/JJ23oRcbAE8kBieJuCBnDuRGUuf9kbyMxHSQv/uzJ91er1U+XDE4M7Lk
/wHWpIhvjhQMxZYXekSg3rvacn1pPqA+m4z6tW5uL2tYi1M99SMSKVXIXtKKRFN+
cQCE5gDsLjn9TYQBRwR3XPedBlqS5UAvpACDLblOfTRaWFr1wGosJ1VlTHwEn768
y1u+IdG8JDbMr9XE93cUZ+M7ijUkF0GY+zATmXfnV1+JtbJznXQe5cWbP/dU3vwr
tVH/UCuH5LlOJ5S7/6vhehI09Vd1MpA0ON853Q25GqLzr9mxHn06nmF5Trws40NK
+ippw45xS89kKapRUmYp2ScyuQXQOxQlyz250Z/T/D3MXtSRq/JnI8/tvBWREfQ9
eYBWI5uhmuDMhvIg4/S5KMo+TeQ6xXNNDJ0r+wQzjRbg6zg2ZPi8Lp0ajmD3r9jc
/QARAQABiQIfBBgBAgAJBQJQhhguAhsMAAoJELAqvr+A71ujgsoP/RtKJF+i7GKc
Ft2EZJK6R3lZwK/oBaurcoV8Q4HcoB+IEuCzrG4K+9cBmlSTzHW6Dk1X8r1xONmu
OZ7hcckaC6mSUUU99rADs9e4l2tgk8MolPO78lr5kDsz5HImf+x/q5Vt0ANKS3Eb
08LqgsuavY/4i/a07Nu3gStw3aOgRCV+Jg4WGVkVMgtk/dhGFSTHW8PsZCM6EFpb
W1xAfiuTcQmBEuxNHgD7ASTZE2wsLKoNhn5mYSqQrpLy3sSsr3w9tfuttPs4rejs
14h7X5qN47eCYXp7kCa2WjZVxJGIkk3VuwmPSDEbdZH6weEZW53PYY6g94oTzd+I
aOb8QeU6+JdwIBaMMkw/Uusc2h5f79AWgumOXlCJ8Zvv8cxI6iQ5hLT/ux3esNKT
A231SNSH8MvHSGgtMY0INFyjTe7kAiax1sxWHoLSmxsLjZXnCVZIimHj2sUfoBDr
+XlMKjOpE66850sRrJk3OZAsS515D7r3+YlFUYrB29ovpM1gYoxFh3C/3XqxGHHI
2/bzdcsUvZczn3FQma3JYARea4Rf2aUxh6a9mRDJieIvu7/oPhQXOOFHqiLsN0Qb
BtPBbli62I94oTzI/BR4mvjCU1ZWPw97/SfXfA8V1ECqY0v9CKJbYh8LxDeMYTxu
MLk7lpo7yBCqa6O9jrNh+jxnYxMFwVpi
=+wzF
-----END PGP PUBLIC KEY BLOCK-----
